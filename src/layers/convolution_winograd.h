// Author:Tun Ban
// Date:2022.6
#ifndef __CONVOLUTION_WINOGRAD_H
#define __CONVOLUTION_WINOGRAD_H

#include "layer.h"

namespace esnn
{

  // winograd param
  static float G[12] = {1, 0, 0, 0.5, 0.5, 0.5, 0.5, -0.5, 0.5, 0, 0, 1}; ///////
  static float GT[12] = {1, 0.5, 0.5, 0, 0, 0.5, -0.5, 0, 0, 0.5, 0.5, 1};
  static float BT[16] = {1, 0, -1, 0, 0, 1, 1, 0, 0, -1, 1, 0, 0, 1, 0, -1};
  static float B[16] = {1, 0, 0, 0, 0, 1, -1, 1, -1, 1, 1, 0, 0, 0, 0, -1};
  static float AT[8] = {1, 1, 1, 0, 0, 1, -1, -1};
  static float A[8] = {1, 0, 1, 1, 1, -1, 0, -1};

  class Convolution_winograd : public Layer
  {
  public:
    // functions
    Convolution_winograd(std::string name = "");
    virtual ~Convolution_winograd();
    virtual int loadParam(std::vector<std::string> params, int offset = 0);
    virtual int loadModel(std::ifstream &fp);
    virtual std::vector<std::vector<int>> inferShape(std::vector<std::vector<int>> bottom_shapes);
    virtual int forward(std::vector<Blob *> &bottoms, std::vector<Blob *> &tops);
    void transform_input(const float *input, float *input_transformed);
    void odot_add(const float *kernel_transformed, const float *input_transformed, float *M_total);
    void transform_output(const float *M_total, float *output);
    // variable
    int kernel;
    int stride;
    int pad;
    int bias;
    int pad_left, pad_right, pad_top, pad_down;
    int output_h;
    int output_w;
    int output_c;
    int input_c;
    Blob *padded;
    Blob *conv_weight;
    Blob *bias_weight;
    int overlap;
    int nw;
    int nh;
    int N;
    int C_in;
    int In_H;
    int In_W;
    int K;
    int detect_if_output_odd;
  };

} // namespace esnn

#endif // __CONVOLUTION_WINOGRAD_H