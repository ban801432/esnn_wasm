// Author:Tun Ban
// Date:2022.6
#include "convolution_winograd.h"
#include "padding.h"

namespace esnn
{

    void Convolution_winograd::transform_input(const float *input, float *input_transformed)
    {
        for (int n = 0; n < N; n++)
        {
            int N_w = n % nw * overlap;
            int N_h = n / nh * overlap;
            // handle different tile
            if ((detect_if_output_odd == 0) || ((N_w != In_W - 3) && (N_h != In_H - 3)))
            {
                for (int c = 0; c < C_in; c++)
                {
                    const float *d_c_n = input + c * In_H * In_W + N_h * In_W + N_w;
                    // BTd = BT * d_c_n
                    float BTd[16] = {0};
                    BTd[0] = BT[0] * d_c_n[0] + BT[1] * d_c_n[0 + In_W] + BT[2] * d_c_n[0 + 2 * In_W] + BT[3] * d_c_n[0 + 3 * In_W];
                    BTd[1] = BT[0] * d_c_n[1] + BT[1] * d_c_n[1 + In_W] + BT[2] * d_c_n[1 + 2 * In_W] + BT[3] * d_c_n[1 + 3 * In_W];
                    BTd[2] = BT[0] * d_c_n[2] + BT[1] * d_c_n[2 + In_W] + BT[2] * d_c_n[2 + 2 * In_W] + BT[3] * d_c_n[2 + 3 * In_W];
                    BTd[3] = BT[0] * d_c_n[3] + BT[1] * d_c_n[3 + In_W] + BT[2] * d_c_n[3 + 2 * In_W] + BT[3] * d_c_n[3 + 3 * In_W];
                    BTd[4] = BT[4] * d_c_n[0] + BT[5] * d_c_n[0 + In_W] + BT[6] * d_c_n[0 + 2 * In_W] + BT[7] * d_c_n[0 + 3 * In_W];
                    BTd[5] = BT[4] * d_c_n[1] + BT[5] * d_c_n[1 + In_W] + BT[6] * d_c_n[1 + 2 * In_W] + BT[7] * d_c_n[1 + 3 * In_W];
                    BTd[6] = BT[4] * d_c_n[2] + BT[5] * d_c_n[2 + In_W] + BT[6] * d_c_n[2 + 2 * In_W] + BT[7] * d_c_n[2 + 3 * In_W];
                    BTd[7] = BT[4] * d_c_n[3] + BT[5] * d_c_n[3 + In_W] + BT[6] * d_c_n[3 + 2 * In_W] + BT[7] * d_c_n[3 + 3 * In_W];
                    BTd[8] = BT[8] * d_c_n[0] + BT[9] * d_c_n[0 + In_W] + BT[10] * d_c_n[0 + 2 * In_W] + BT[11] * d_c_n[0 + 3 * In_W];
                    BTd[9] = BT[8] * d_c_n[1] + BT[9] * d_c_n[1 + In_W] + BT[10] * d_c_n[1 + 2 * In_W] + BT[11] * d_c_n[1 + 3 * In_W];
                    BTd[10] = BT[8] * d_c_n[2] + BT[9] * d_c_n[2 + In_W] + BT[10] * d_c_n[2 + 2 * In_W] + BT[11] * d_c_n[2 + 3 * In_W];
                    BTd[11] = BT[8] * d_c_n[3] + BT[9] * d_c_n[3 + In_W] + BT[10] * d_c_n[3 + 2 * In_W] + BT[11] * d_c_n[3 + 3 * In_W];
                    BTd[12] = BT[12] * d_c_n[0] + BT[13] * d_c_n[0 + In_W] + BT[14] * d_c_n[0 + 2 * In_W] + BT[15] * d_c_n[0 + 3 * In_W];
                    BTd[13] = BT[12] * d_c_n[1] + BT[13] * d_c_n[1 + In_W] + BT[14] * d_c_n[1 + 2 * In_W] + BT[15] * d_c_n[1 + 3 * In_W];
                    BTd[14] = BT[12] * d_c_n[2] + BT[13] * d_c_n[2 + In_W] + BT[14] * d_c_n[2 + 2 * In_W] + BT[15] * d_c_n[2 + 3 * In_W];
                    BTd[15] = BT[12] * d_c_n[3] + BT[13] * d_c_n[3 + In_W] + BT[14] * d_c_n[3 + 2 * In_W] + BT[15] * d_c_n[3 + 3 * In_W];
                    // BTdB = BTd * B
                    float BTdB[16] = {0};
                    BTdB[0] = BTd[0] * B[0] + BTd[1] * B[4] + BTd[2] * B[8] + BTd[3] * B[12];
                    BTdB[1] = BTd[0] * B[1] + BTd[1] * B[5] + BTd[2] * B[9] + BTd[3] * B[13];
                    BTdB[2] = BTd[0] * B[2] + BTd[1] * B[6] + BTd[2] * B[10] + BTd[3] * B[14];
                    BTdB[3] = BTd[0] * B[3] + BTd[1] * B[7] + BTd[2] * B[11] + BTd[3] * B[15];
                    BTdB[4] = BTd[4] * B[0] + BTd[5] * B[4] + BTd[6] * B[8] + BTd[7] * B[12];
                    BTdB[5] = BTd[4] * B[1] + BTd[5] * B[5] + BTd[6] * B[9] + BTd[7] * B[13];
                    BTdB[6] = BTd[4] * B[2] + BTd[5] * B[6] + BTd[6] * B[10] + BTd[7] * B[14];
                    BTdB[7] = BTd[4] * B[3] + BTd[5] * B[7] + BTd[6] * B[11] + BTd[7] * B[15];
                    BTdB[8] = BTd[8] * B[0] + BTd[9] * B[4] + BTd[10] * B[8] + BTd[11] * B[12];
                    BTdB[9] = BTd[8] * B[1] + BTd[9] * B[5] + BTd[10] * B[9] + BTd[11] * B[13];
                    BTdB[10] = BTd[8] * B[2] + BTd[9] * B[6] + BTd[10] * B[10] + BTd[11] * B[14];
                    BTdB[11] = BTd[8] * B[3] + BTd[9] * B[7] + BTd[10] * B[11] + BTd[11] * B[15];
                    BTdB[12] = BTd[12] * B[0] + BTd[13] * B[4] + BTd[14] * B[8] + BTd[15] * B[12];
                    BTdB[13] = BTd[12] * B[1] + BTd[13] * B[5] + BTd[14] * B[9] + BTd[15] * B[13];
                    BTdB[14] = BTd[12] * B[2] + BTd[13] * B[6] + BTd[14] * B[10] + BTd[15] * B[14];
                    BTdB[15] = BTd[12] * B[3] + BTd[13] * B[7] + BTd[14] * B[11] + BTd[15] * B[15];
                    for (int i = 0; i < 16; i++)
                    {
                        input_transformed[i + n * C_in * 16 + c * 16] = BTdB[i];
                    }
                }
            }
            // handle different tile
            else if ((N_w == In_W - 3) && (N_h != In_H - 3))
            {
                for (int c = 0; c < C_in; c++)
                {
                    const float *d_c_n = input + c * In_H * In_W + N_h * In_W + N_w;
                    // BTd = BT * d_c_n
                    float BTd[16] = {0};
                    BTd[0] = BT[0] * d_c_n[0] + BT[1] * d_c_n[0 + In_W] + BT[2] * d_c_n[0 + 2 * In_W] + BT[3] * d_c_n[0 + 3 * In_W];
                    BTd[1] = BT[0] * d_c_n[1] + BT[1] * d_c_n[1 + In_W] + BT[2] * d_c_n[1 + 2 * In_W] + BT[3] * d_c_n[1 + 3 * In_W];
                    BTd[2] = BT[0] * d_c_n[2] + BT[1] * d_c_n[2 + In_W] + BT[2] * d_c_n[2 + 2 * In_W] + BT[3] * d_c_n[2 + 3 * In_W];
                    // BTd[3]=BT[0]*d_c_n[3]+BT[1]*d_c_n[3+In_W]+BT[2]*d_c_n[3+2*In_W]+BT[3]*d_c_n[3+3*In_W];
                    BTd[3] = 0;
                    BTd[4] = BT[4] * d_c_n[0] + BT[5] * d_c_n[0 + In_W] + BT[6] * d_c_n[0 + 2 * In_W] + BT[7] * d_c_n[0 + 3 * In_W];
                    BTd[5] = BT[4] * d_c_n[1] + BT[5] * d_c_n[1 + In_W] + BT[6] * d_c_n[1 + 2 * In_W] + BT[7] * d_c_n[1 + 3 * In_W];
                    BTd[6] = BT[4] * d_c_n[2] + BT[5] * d_c_n[2 + In_W] + BT[6] * d_c_n[2 + 2 * In_W] + BT[7] * d_c_n[2 + 3 * In_W];
                    // BTd[7]=BT[4]*d_c_n[3]+BT[5]*d_c_n[3+In_W]+BT[6]*d_c_n[3+2*In_W]+BT[7]*d_c_n[3+3*In_W];
                    BTd[7] = 0;
                    BTd[8] = BT[8] * d_c_n[0] + BT[9] * d_c_n[0 + In_W] + BT[10] * d_c_n[0 + 2 * In_W] + BT[11] * d_c_n[0 + 3 * In_W];
                    BTd[9] = BT[8] * d_c_n[1] + BT[9] * d_c_n[1 + In_W] + BT[10] * d_c_n[1 + 2 * In_W] + BT[11] * d_c_n[1 + 3 * In_W];
                    BTd[10] = BT[8] * d_c_n[2] + BT[9] * d_c_n[2 + In_W] + BT[10] * d_c_n[2 + 2 * In_W] + BT[11] * d_c_n[2 + 3 * In_W];
                    // BTd[11]=BT[8]*d_c_n[3]+BT[9]*d_c_n[3+In_W]+BT[10]*d_c_n[3+2*In_W]+BT[11]*d_c_n[3+3*In_W];
                    BTd[11] = 0;
                    BTd[12] = BT[12] * d_c_n[0] + BT[13] * d_c_n[0 + In_W] + BT[14] * d_c_n[0 + 2 * In_W] + BT[15] * d_c_n[0 + 3 * In_W];
                    BTd[13] = BT[12] * d_c_n[1] + BT[13] * d_c_n[1 + In_W] + BT[14] * d_c_n[1 + 2 * In_W] + BT[15] * d_c_n[1 + 3 * In_W];
                    BTd[14] = BT[12] * d_c_n[2] + BT[13] * d_c_n[2 + In_W] + BT[14] * d_c_n[2 + 2 * In_W] + BT[15] * d_c_n[2 + 3 * In_W];
                    // BTd[15]=BT[12]*d_c_n[3]+BT[13]*d_c_n[3+In_W]+BT[14]*d_c_n[3+2*In_W]+BT[15]*d_c_n[3+3*In_W];
                    BTd[15] = 0;
                    // BTdB = BTd * B
                    float BTdB[16] = {0};
                    BTdB[0] = BTd[0] * B[0] + BTd[1] * B[4] + BTd[2] * B[8] + BTd[3] * B[12];
                    BTdB[1] = BTd[0] * B[1] + BTd[1] * B[5] + BTd[2] * B[9] + BTd[3] * B[13];
                    BTdB[2] = BTd[0] * B[2] + BTd[1] * B[6] + BTd[2] * B[10] + BTd[3] * B[14];
                    BTdB[3] = BTd[0] * B[3] + BTd[1] * B[7] + BTd[2] * B[11] + BTd[3] * B[15];
                    BTdB[4] = BTd[4] * B[0] + BTd[5] * B[4] + BTd[6] * B[8] + BTd[7] * B[12];
                    BTdB[5] = BTd[4] * B[1] + BTd[5] * B[5] + BTd[6] * B[9] + BTd[7] * B[13];
                    BTdB[6] = BTd[4] * B[2] + BTd[5] * B[6] + BTd[6] * B[10] + BTd[7] * B[14];
                    BTdB[7] = BTd[4] * B[3] + BTd[5] * B[7] + BTd[6] * B[11] + BTd[7] * B[15];
                    BTdB[8] = BTd[8] * B[0] + BTd[9] * B[4] + BTd[10] * B[8] + BTd[11] * B[12];
                    BTdB[9] = BTd[8] * B[1] + BTd[9] * B[5] + BTd[10] * B[9] + BTd[11] * B[13];
                    BTdB[10] = BTd[8] * B[2] + BTd[9] * B[6] + BTd[10] * B[10] + BTd[11] * B[14];
                    BTdB[11] = BTd[8] * B[3] + BTd[9] * B[7] + BTd[10] * B[11] + BTd[11] * B[15];
                    BTdB[12] = BTd[12] * B[0] + BTd[13] * B[4] + BTd[14] * B[8] + BTd[15] * B[12];
                    BTdB[13] = BTd[12] * B[1] + BTd[13] * B[5] + BTd[14] * B[9] + BTd[15] * B[13];
                    BTdB[14] = BTd[12] * B[2] + BTd[13] * B[6] + BTd[14] * B[10] + BTd[15] * B[14];
                    BTdB[15] = BTd[12] * B[3] + BTd[13] * B[7] + BTd[14] * B[11] + BTd[15] * B[15];
                    for (int i = 0; i < 16; i++)
                    {
                        input_transformed[i + n * C_in * 16 + c * 16] = BTdB[i];
                    }
                }
            }
            // handle different tile
            else if ((N_w != In_W - 3) && (N_h == In_H - 3))
            {
                for (int c = 0; c < C_in; c++)
                {
                    const float *d_c_n = input + c * In_H * In_W + N_h * In_W + N_w;
                    // BTd = BT * d_c_n
                    float BTd[16] = {0};
                    BTd[0] = BT[0] * d_c_n[0] + BT[1] * d_c_n[0 + In_W] + BT[2] * d_c_n[0 + 2 * In_W];     // BT[3]*d_c_n[0+3*In_W];
                    BTd[1] = BT[0] * d_c_n[1] + BT[1] * d_c_n[1 + In_W] + BT[2] * d_c_n[1 + 2 * In_W];     //+BT[3]*d_c_n[1+3*In_W];
                    BTd[2] = BT[0] * d_c_n[2] + BT[1] * d_c_n[2 + In_W] + BT[2] * d_c_n[2 + 2 * In_W];     //+BT[3]*d_c_n[2+3*In_W];
                    BTd[3] = BT[0] * d_c_n[3] + BT[1] * d_c_n[3 + In_W] + BT[2] * d_c_n[3 + 2 * In_W];     //+BT[3]*d_c_n[3+3*In_W];
                    BTd[4] = BT[4] * d_c_n[0] + BT[5] * d_c_n[0 + In_W] + BT[6] * d_c_n[0 + 2 * In_W];     //+BT[7]*d_c_n[0+3*In_W];
                    BTd[5] = BT[4] * d_c_n[1] + BT[5] * d_c_n[1 + In_W] + BT[6] * d_c_n[1 + 2 * In_W];     //+BT[7]*d_c_n[1+3*In_W];
                    BTd[6] = BT[4] * d_c_n[2] + BT[5] * d_c_n[2 + In_W] + BT[6] * d_c_n[2 + 2 * In_W];     //+BT[7]*d_c_n[2+3*In_W];
                    BTd[7] = BT[4] * d_c_n[3] + BT[5] * d_c_n[3 + In_W] + BT[6] * d_c_n[3 + 2 * In_W];     //+BT[7]*d_c_n[3+3*In_W];
                    BTd[8] = BT[8] * d_c_n[0] + BT[9] * d_c_n[0 + In_W] + BT[10] * d_c_n[0 + 2 * In_W];    //+BT[11]*d_c_n[0+3*In_W];
                    BTd[9] = BT[8] * d_c_n[1] + BT[9] * d_c_n[1 + In_W] + BT[10] * d_c_n[1 + 2 * In_W];    //+BT[11]*d_c_n[1+3*In_W];
                    BTd[10] = BT[8] * d_c_n[2] + BT[9] * d_c_n[2 + In_W] + BT[10] * d_c_n[2 + 2 * In_W];   //+BT[11]*d_c_n[2+3*In_W];
                    BTd[11] = BT[8] * d_c_n[3] + BT[9] * d_c_n[3 + In_W] + BT[10] * d_c_n[3 + 2 * In_W];   //+BT[11]*d_c_n[3+3*In_W];
                    BTd[12] = BT[12] * d_c_n[0] + BT[13] * d_c_n[0 + In_W] + BT[14] * d_c_n[0 + 2 * In_W]; //+BT[15]*d_c_n[0+3*In_W];
                    BTd[13] = BT[12] * d_c_n[1] + BT[13] * d_c_n[1 + In_W] + BT[14] * d_c_n[1 + 2 * In_W]; //+BT[15]*d_c_n[1+3*In_W];
                    BTd[14] = BT[12] * d_c_n[2] + BT[13] * d_c_n[2 + In_W] + BT[14] * d_c_n[2 + 2 * In_W]; //+BT[15]*d_c_n[2+3*In_W];
                    BTd[15] = BT[12] * d_c_n[3] + BT[13] * d_c_n[3 + In_W] + BT[14] * d_c_n[3 + 2 * In_W]; //+BT[15]*d_c_n[3+3*In_W];
                    // BTdB = BTd * B
                    float BTdB[16] = {0};
                    BTdB[0] = BTd[0] * B[0] + BTd[1] * B[4] + BTd[2] * B[8] + BTd[3] * B[12];
                    BTdB[1] = BTd[0] * B[1] + BTd[1] * B[5] + BTd[2] * B[9] + BTd[3] * B[13];
                    BTdB[2] = BTd[0] * B[2] + BTd[1] * B[6] + BTd[2] * B[10] + BTd[3] * B[14];
                    BTdB[3] = BTd[0] * B[3] + BTd[1] * B[7] + BTd[2] * B[11] + BTd[3] * B[15];
                    BTdB[4] = BTd[4] * B[0] + BTd[5] * B[4] + BTd[6] * B[8] + BTd[7] * B[12];
                    BTdB[5] = BTd[4] * B[1] + BTd[5] * B[5] + BTd[6] * B[9] + BTd[7] * B[13];
                    BTdB[6] = BTd[4] * B[2] + BTd[5] * B[6] + BTd[6] * B[10] + BTd[7] * B[14];
                    BTdB[7] = BTd[4] * B[3] + BTd[5] * B[7] + BTd[6] * B[11] + BTd[7] * B[15];
                    BTdB[8] = BTd[8] * B[0] + BTd[9] * B[4] + BTd[10] * B[8] + BTd[11] * B[12];
                    BTdB[9] = BTd[8] * B[1] + BTd[9] * B[5] + BTd[10] * B[9] + BTd[11] * B[13];
                    BTdB[10] = BTd[8] * B[2] + BTd[9] * B[6] + BTd[10] * B[10] + BTd[11] * B[14];
                    BTdB[11] = BTd[8] * B[3] + BTd[9] * B[7] + BTd[10] * B[11] + BTd[11] * B[15];
                    BTdB[12] = BTd[12] * B[0] + BTd[13] * B[4] + BTd[14] * B[8] + BTd[15] * B[12];
                    BTdB[13] = BTd[12] * B[1] + BTd[13] * B[5] + BTd[14] * B[9] + BTd[15] * B[13];
                    BTdB[14] = BTd[12] * B[2] + BTd[13] * B[6] + BTd[14] * B[10] + BTd[15] * B[14];
                    BTdB[15] = BTd[12] * B[3] + BTd[13] * B[7] + BTd[14] * B[11] + BTd[15] * B[15];
                    for (int i = 0; i < 16; i++)
                    {
                        input_transformed[i + n * C_in * 16 + c * 16] = BTdB[i];
                    }
                }
            }
            // handle different tile
            else if ((N_w == In_W - 3) && (N_h == In_H - 3))
            {
                for (int c = 0; c < C_in; c++)
                {
                    const float *d_c_n = input + c * In_H * In_W + N_h * In_W + N_w;
                    // BTd = BT * d_c_n
                    float BTd[16] = {0};
                    BTd[0] = BT[0] * d_c_n[0] + BT[1] * d_c_n[0 + In_W] + BT[2] * d_c_n[0 + 2 * In_W];     //+BT[3]*d_c_n[0+3*In_W];
                    BTd[1] = BT[0] * d_c_n[1] + BT[1] * d_c_n[1 + In_W] + BT[2] * d_c_n[1 + 2 * In_W];     //+BT[3]*d_c_n[1+3*In_W];
                    BTd[2] = BT[0] * d_c_n[2] + BT[1] * d_c_n[2 + In_W] + BT[2] * d_c_n[2 + 2 * In_W];     //+BT[3]*d_c_n[2+3*In_W];
                    BTd[3] = 0;                                                                            // BT[0]*d_c_n[3]+BT[1]*d_c_n[3+In_W]+BT[2]*d_c_n[3+2*In_W];//+BT[3]*d_c_n[3+3*In_W];
                    BTd[4] = BT[4] * d_c_n[0] + BT[5] * d_c_n[0 + In_W] + BT[6] * d_c_n[0 + 2 * In_W];     //+BT[7]*d_c_n[0+3*In_W];
                    BTd[5] = BT[4] * d_c_n[1] + BT[5] * d_c_n[1 + In_W] + BT[6] * d_c_n[1 + 2 * In_W];     //+BT[7]*d_c_n[1+3*In_W];
                    BTd[6] = BT[4] * d_c_n[2] + BT[5] * d_c_n[2 + In_W] + BT[6] * d_c_n[2 + 2 * In_W];     //+BT[7]*d_c_n[2+3*In_W];
                    BTd[7] = 0;                                                                            //=BT[4]*d_c_n[3]+BT[5]*d_c_n[3+In_W]+BT[6]*d_c_n[3+2*In_W];//+BT[7]*d_c_n[3+3*In_W];
                    BTd[8] = BT[8] * d_c_n[0] + BT[9] * d_c_n[0 + In_W] + BT[10] * d_c_n[0 + 2 * In_W];    //+BT[11]*d_c_n[0+3*In_W];
                    BTd[9] = BT[8] * d_c_n[1] + BT[9] * d_c_n[1 + In_W] + BT[10] * d_c_n[1 + 2 * In_W];    //+BT[11]*d_c_n[1+3*In_W];
                    BTd[10] = BT[8] * d_c_n[2] + BT[9] * d_c_n[2 + In_W] + BT[10] * d_c_n[2 + 2 * In_W];   //+BT[11]*d_c_n[2+3*In_W];
                    BTd[11] = 0;                                                                           //=BT[8]*d_c_n[3]+BT[9]*d_c_n[3+In_W]+BT[10]*d_c_n[3+2*In_W];//+BT[11]*d_c_n[3+3*In_W];
                    BTd[12] = BT[12] * d_c_n[0] + BT[13] * d_c_n[0 + In_W] + BT[14] * d_c_n[0 + 2 * In_W]; //+BT[15]*d_c_n[0+3*In_W];
                    BTd[13] = BT[12] * d_c_n[1] + BT[13] * d_c_n[1 + In_W] + BT[14] * d_c_n[1 + 2 * In_W]; //+BT[15]*d_c_n[1+3*In_W];
                    BTd[14] = BT[12] * d_c_n[2] + BT[13] * d_c_n[2 + In_W] + BT[14] * d_c_n[2 + 2 * In_W]; //+BT[15]*d_c_n[2+3*In_W];
                    BTd[15] = 0;                                                                           //=BT[12]*d_c_n[3]+BT[13]*d_c_n[3+In_W]+BT[14]*d_c_n[3+2*In_W];//+BT[15]*d_c_n[3+3*In_W];
                    // BTdB = BTd * B
                    float BTdB[16] = {0};
                    BTdB[0] = BTd[0] * B[0] + BTd[1] * B[4] + BTd[2] * B[8] + BTd[3] * B[12];
                    BTdB[1] = BTd[0] * B[1] + BTd[1] * B[5] + BTd[2] * B[9] + BTd[3] * B[13];
                    BTdB[2] = BTd[0] * B[2] + BTd[1] * B[6] + BTd[2] * B[10] + BTd[3] * B[14];
                    BTdB[3] = BTd[0] * B[3] + BTd[1] * B[7] + BTd[2] * B[11] + BTd[3] * B[15];
                    BTdB[4] = BTd[4] * B[0] + BTd[5] * B[4] + BTd[6] * B[8] + BTd[7] * B[12];
                    BTdB[5] = BTd[4] * B[1] + BTd[5] * B[5] + BTd[6] * B[9] + BTd[7] * B[13];
                    BTdB[6] = BTd[4] * B[2] + BTd[5] * B[6] + BTd[6] * B[10] + BTd[7] * B[14];
                    BTdB[7] = BTd[4] * B[3] + BTd[5] * B[7] + BTd[6] * B[11] + BTd[7] * B[15];
                    BTdB[8] = BTd[8] * B[0] + BTd[9] * B[4] + BTd[10] * B[8] + BTd[11] * B[12];
                    BTdB[9] = BTd[8] * B[1] + BTd[9] * B[5] + BTd[10] * B[9] + BTd[11] * B[13];
                    BTdB[10] = BTd[8] * B[2] + BTd[9] * B[6] + BTd[10] * B[10] + BTd[11] * B[14];
                    BTdB[11] = BTd[8] * B[3] + BTd[9] * B[7] + BTd[10] * B[11] + BTd[11] * B[15];
                    BTdB[12] = BTd[12] * B[0] + BTd[13] * B[4] + BTd[14] * B[8] + BTd[15] * B[12];
                    BTdB[13] = BTd[12] * B[1] + BTd[13] * B[5] + BTd[14] * B[9] + BTd[15] * B[13];
                    BTdB[14] = BTd[12] * B[2] + BTd[13] * B[6] + BTd[14] * B[10] + BTd[15] * B[14];
                    BTdB[15] = BTd[12] * B[3] + BTd[13] * B[7] + BTd[14] * B[11] + BTd[15] * B[15];
                    for (int i = 0; i < 16; i++)
                    {
                        input_transformed[i + n * C_in * 16 + c * 16] = BTdB[i];
                    }
                }
            }
            else
            {
                INVALID_ERROR("ERROR!\n");
            }
        }
    }

    void Convolution_winograd::odot_add(const float *kernel_transformed, const float *input_transformed, float *M_total)
    {
        for (int k = 0; k < K; k++)
        {
            for (int n = 0; n < N; n++)
            {
                for (int i = 0; i < 16; i++)
                {
                    float data_point = 0;
                    for (int j = 0; j < C_in; j++)
                    {
                        data_point += kernel_transformed[k * C_in * 16 + j * 16 + i] * input_transformed[n * C_in * 16 + j * 16 + i];
                    }
                    M_total[i + k * N * 16 + n * 16] = data_point;
                }
            }
        }
    }

    void Convolution_winograd::transform_output(const float *M_total, float *output)
    {
        for (int k = 0; k < K; k++)
        {
            for (int n = 0; n < N; n++)
            {
                int N_w = n % nw * overlap;
                int N_h = n / nw * overlap;
                int num = k * N * 16 + n * 16;
                // handle different tile
                if ((detect_if_output_odd == 0) || ((N_w != In_W - 3) && (N_h != In_H - 3)))
                {
                    float ATM[8] = {0};
                    ATM[0] = AT[0] * M_total[num] + AT[1] * M_total[num + 4] + AT[2] * M_total[num + 8] + AT[3] * M_total[num + 12];
                    ATM[1] = AT[0] * M_total[num + 1] + AT[1] * M_total[num + 5] + AT[2] * M_total[num + 9] + AT[3] * M_total[num + 13];
                    ATM[2] = AT[0] * M_total[num + 2] + AT[1] * M_total[num + 6] + AT[2] * M_total[num + 10] + AT[3] * M_total[num + 14];
                    ATM[3] = AT[0] * M_total[num + 3] + AT[1] * M_total[num + 7] + AT[2] * M_total[num + 11] + AT[3] * M_total[num + 15];
                    ATM[4] = AT[4] * M_total[num] + AT[5] * M_total[num + 4] + AT[6] * M_total[num + 8] + AT[7] * M_total[num + 12];
                    ATM[5] = AT[4] * M_total[num + 1] + AT[5] * M_total[num + 5] + AT[6] * M_total[num + 9] + AT[7] * M_total[num + 13];
                    ATM[6] = AT[4] * M_total[num + 2] + AT[5] * M_total[num + 6] + AT[6] * M_total[num + 10] + AT[7] * M_total[num + 14];
                    ATM[7] = AT[4] * M_total[num + 3] + AT[5] * M_total[num + 7] + AT[6] * M_total[num + 11] + AT[7] * M_total[num + 15];
                    // ATMA = ATM*A
                    float ATMA[4] = {0};
                    ATMA[0] = ATM[0] * A[0] + ATM[1] * A[2] + ATM[2] * A[4] + ATM[3] * A[6];
                    ATMA[1] = ATM[0] * A[1] + ATM[1] * A[3] + ATM[2] * A[5] + ATM[3] * A[7];
                    ATMA[2] = ATM[4] * A[0] + ATM[5] * A[2] + ATM[6] * A[4] + ATM[7] * A[6];
                    ATMA[3] = ATM[4] * A[1] + ATM[5] * A[3] + ATM[6] * A[5] + ATM[7] * A[7];
                    int numnum = k * output_h * output_h + N_h * output_h + N_w; // change
                    output[numnum] = ATMA[0];
                    output[numnum + 1] = ATMA[1];
                    output[numnum + output_h] = ATMA[2];
                    output[numnum + 1 + output_h] = ATMA[3];
                }
                // handle different tile
                else if ((N_w == In_W - 3) && (N_h != In_H - 3))
                {
                    float ATM[8] = {0};
                    ATM[0] = AT[0] * M_total[num] + AT[1] * M_total[num + 4] + AT[2] * M_total[num + 8] + AT[3] * M_total[num + 12];
                    ATM[1] = AT[0] * M_total[num + 1] + AT[1] * M_total[num + 5] + AT[2] * M_total[num + 9] + AT[3] * M_total[num + 13];
                    ATM[2] = AT[0] * M_total[num + 2] + AT[1] * M_total[num + 6] + AT[2] * M_total[num + 10] + AT[3] * M_total[num + 14];
                    ATM[3] = AT[0] * M_total[num + 3] + AT[1] * M_total[num + 7] + AT[2] * M_total[num + 11] + AT[3] * M_total[num + 15];
                    ATM[4] = AT[4] * M_total[num] + AT[5] * M_total[num + 4] + AT[6] * M_total[num + 8] + AT[7] * M_total[num + 12];
                    ATM[5] = AT[4] * M_total[num + 1] + AT[5] * M_total[num + 5] + AT[6] * M_total[num + 9] + AT[7] * M_total[num + 13];
                    ATM[6] = AT[4] * M_total[num + 2] + AT[5] * M_total[num + 6] + AT[6] * M_total[num + 10] + AT[7] * M_total[num + 14];
                    ATM[7] = AT[4] * M_total[num + 3] + AT[5] * M_total[num + 7] + AT[6] * M_total[num + 11] + AT[7] * M_total[num + 15];
                    // ATMA = ATM*A
                    float ATMA[4] = {0};
                    ATMA[0] = ATM[0] * A[0] + ATM[1] * A[2] + ATM[2] * A[4] + ATM[3] * A[6];
                    ATMA[2] = ATM[4] * A[0] + ATM[5] * A[2] + ATM[6] * A[4] + ATM[7] * A[6];
                    int numnum = k * output_h * output_h + N_h * output_h + N_w; // change
                    output[numnum] = ATMA[0];
                    output[numnum + output_h] = ATMA[2];
                }
                // handle different tile
                else if ((N_w != In_W - 3) && (N_h == In_H - 3))
                {
                    float ATM[8] = {0};
                    ATM[0] = AT[0] * M_total[num] + AT[1] * M_total[num + 4] + AT[2] * M_total[num + 8] + AT[3] * M_total[num + 12];
                    ATM[1] = AT[0] * M_total[num + 1] + AT[1] * M_total[num + 5] + AT[2] * M_total[num + 9] + AT[3] * M_total[num + 13];
                    ATM[2] = AT[0] * M_total[num + 2] + AT[1] * M_total[num + 6] + AT[2] * M_total[num + 10] + AT[3] * M_total[num + 14];
                    ATM[3] = AT[0] * M_total[num + 3] + AT[1] * M_total[num + 7] + AT[2] * M_total[num + 11] + AT[3] * M_total[num + 15];
                    ATM[4] = AT[4] * M_total[num] + AT[5] * M_total[num + 4] + AT[6] * M_total[num + 8] + AT[7] * M_total[num + 12];
                    ATM[5] = AT[4] * M_total[num + 1] + AT[5] * M_total[num + 5] + AT[6] * M_total[num + 9] + AT[7] * M_total[num + 13];
                    ATM[6] = AT[4] * M_total[num + 2] + AT[5] * M_total[num + 6] + AT[6] * M_total[num + 10] + AT[7] * M_total[num + 14];
                    ATM[7] = AT[4] * M_total[num + 3] + AT[5] * M_total[num + 7] + AT[6] * M_total[num + 11] + AT[7] * M_total[num + 15];
                    // ATMA = ATM*A
                    float ATMA[4] = {0};
                    ATMA[0] = ATM[0] * A[0] + ATM[1] * A[2] + ATM[2] * A[4] + ATM[3] * A[6];
                    ATMA[1] = ATM[0] * A[1] + ATM[1] * A[3] + ATM[2] * A[5] + ATM[3] * A[7];
                    int numnum = k * output_h * output_h + N_h * output_h + N_w; // change
                    output[numnum] = ATMA[0];
                    output[numnum + 1] = ATMA[1];
                }
                // handle different tile
                else if ((N_w == In_W - 3) && (N_h == In_H - 3))
                {
                    float ATM[8] = {0};
                    ATM[0] = AT[0] * M_total[num] + AT[1] * M_total[num + 4] + AT[2] * M_total[num + 8] + AT[3] * M_total[num + 12];
                    ATM[1] = AT[0] * M_total[num + 1] + AT[1] * M_total[num + 5] + AT[2] * M_total[num + 9] + AT[3] * M_total[num + 13];
                    ATM[2] = AT[0] * M_total[num + 2] + AT[1] * M_total[num + 6] + AT[2] * M_total[num + 10] + AT[3] * M_total[num + 14];
                    ATM[3] = AT[0] * M_total[num + 3] + AT[1] * M_total[num + 7] + AT[2] * M_total[num + 11] + AT[3] * M_total[num + 15];
                    // ATMA = ATM*A
                    float ATMA[4] = {0};
                    ATMA[0] = ATM[0] * A[0] + ATM[1] * A[2] + ATM[2] * A[4] + ATM[3] * A[6];
                    int numnum = k * output_h * output_h + N_h * output_h + N_w; // change
                    output[numnum] = ATMA[0];
                }
                else
                {
                    INVALID_ERROR("ERROR!\n");
                }
            }
        }
    }

    Convolution_winograd::Convolution_winograd(std::string name_) : Layer(name_), padded(NULL), conv_weight(NULL), bias_weight(NULL) { type = "Convolution"; }

    Convolution_winograd::~Convolution_winograd()
    {
        if (padded != NULL)
        {
            delete padded;
        }
        if (conv_weight != NULL)
        {
            delete conv_weight;
        }
        if (bias_weight != NULL)
        {
            delete bias_weight;
        }
    }

    int Convolution_winograd::loadModel(std::ifstream &fp)
    {
        fp.read((char *)(conv_weight->getPtr()), conv_weight->size * sizeof(float));
        return 0;
    }

    int Convolution_winograd::loadParam(std::vector<std::string> params, int offset)
    {
        kernel = string2int(params[offset]);
        stride = string2int(params[offset + 1]);
        output_c = string2int(params[offset + 2]);
        pad = string2int(params[offset + 3]);
        bias = string2int(params[offset + 4]);
        return 0;
    }

    std::vector<std::vector<int>> Convolution_winograd::inferShape(std::vector<std::vector<int>> bottom_shapes)
    {
        std::vector<int> bottom_shape = bottom_shapes[0];
        if (bottom_shape.size() != 3)
        {
            INVALID_ERROR("shape should be (h,w,c)!\n");
        }
        // get correct pad
        pad_left = pad_right = pad_top = pad_down = pad;
        int padded_h, padded_w;
        filter_padding_calculate(bottom_shape[0], bottom_shape[1], kernel, stride,
                                 pad_left, pad_right, pad_top, pad_down,
                                 padded_h, padded_w);
        int padded_c = bottom_shape[2];
        input_c = padded_c;
        if (pad_left || pad_right || pad_top || pad_down)
        {
            padded = new Blob(padded_h, padded_w, padded_c);
            padded->setValue(0);
        }
        // get output shape
        output_h = (padded_h - kernel) / stride + 1;
        output_w = (padded_w - kernel) / stride + 1;
        std::vector<int> top_shape{output_h, output_w, output_c};
        std::vector<std::vector<int>> top_shapes{top_shape};
        // winograd
        overlap = 2;
        detect_if_output_odd = output_h % 2;
        nw = (detect_if_output_odd == 0) ? (output_h / 2) : (output_h / 2 + 1);
        nh = nw;
        N = nw * nh;
        C_in = input_c;
        In_H = output_h + 2;
        In_W = In_H;
        K = output_c;
        // prepare weight blob
        conv_weight = new Blob(4, 4, input_c * output_c);
        return top_shapes;
    }

    int Convolution_winograd::forward(std::vector<Blob *> &bottoms, std::vector<Blob *> &tops)
    {

        Blob *bottom = bottoms[0];
        Blob *top = tops[0];
        // process padding
        if (padded != NULL)
        {
            padding_forward(bottom, padded, pad_left, pad_right, pad_top, pad_down);
            bottom = padded;
        }
        // start to calculate convolution
        const float *bottom_data = bottom->getPtr();
        const float *conv_weight_data = conv_weight->getPtr();
        // const float* bias_weight_data = bias==1?bias_weight->getPtr():NULL;
        float *top_data = top->getPtr();
        const float *src_offset = bottom_data;
        float *input_transformed = new float[C_in * N * 16];
        transform_input(bottom_data, input_transformed);
        float *M_total = new float[K * N * 16];
        odot_add(conv_weight_data, input_transformed, M_total);
        transform_output(M_total, top_data);
        delete[] input_transformed;
        delete[] M_total;
        return 0;
    }
} // namespace esnn