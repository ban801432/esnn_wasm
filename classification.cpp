// Author:Tun Ban
// Date:2022.6
#include <iostream>
#include <vector>
#include <chrono>
#include "src/blob.h"
#include "src/layer.h"
#include "src/net.h"
#include "emscripten.h"

#define FLT_MIN 1.175494351e-38F
#define FLT_MAX 3.402823466e+38F

static inline int argmax(esnn::Blob &src)
{
  float max_score = -FLT_MAX;
  int max_index = -1;
  const float *src_data = src.getPtr();
  int i = 0;
  for (; i < src.size; i++)
  {
    if (src_data[i] > max_score)
    {
      max_index = i;
      max_score = src_data[i];
    }
  }
  return max_index;
}

extern "C"
{
  EMSCRIPTEN_KEEPALIVE
  uint8_t *create_buffer(int width, int height)
  {
    return static_cast<uint8_t *>(malloc(width * height * 4 * sizeof(uint8_t)));
  }

  EMSCRIPTEN_KEEPALIVE
  void destroy_buffer(uint8_t *p)
  {
    free(p);
  }

  EMSCRIPTEN_KEEPALIVE
  int wineclassification(uint8_t *img_in)
  {
    esnn::Net net;
    std::cout << "start to load param" << std::endl;
    net.loadParam("res6/param_res_6");
    std::cout << "start to load model" << std::endl;
    net.loadModel("res6/model_res_6");
    net.printNetInfo();
    esnn::Blob data_blob = net.getBlob("data");
    std::cout << "start to read image" << std::endl;
    uint8_t img_out_B[224 * 224];
    uint8_t img_out_G[224 * 224];
    uint8_t img_out_R[224 * 224];
    uint8_t *b = img_out_B;
    uint8_t *g = img_out_G;
    uint8_t *r = img_out_R;
    uint8_t mean[3] = {108, 119, 118}; // bgr
    int j = 0;
    for (int i = 0; i < 224 * 224 * 4; i++)
    {
      if (j == 0)
      {
        j += 1;
        *r = (img_in[i] > mean[2]) ? (img_in[i] - mean[2]) : 0;
        r += 1;
      }
      else if (j == 1)
      {
        j += 1;
        *g = (img_in[i] > mean[1]) ? (img_in[i] - mean[1]) : 0;
        g += 1;
      }
      else if (j == 2)
      {
        j += 1;
        *b = (img_in[i] > mean[0]) ? (img_in[i] - mean[0]) : 0;
        b += 1;
      }
      else
      {
        j = 0;
      }
    }
    float *dst_data = data_blob.getPtr();
    for (int i = 0; i < data_blob.h * data_blob.w; i++)
    {
      *(dst_data++) = (float)(*(b++));
    }
    for (int i = 0; i < data_blob.h * data_blob.w; i++)
    {
      *(dst_data++) = (float)(*(g++));
    }
    for (int i = 0; i < data_blob.h * data_blob.w; i++)
    {
      *(dst_data++) = (float)(*(r++));
    }
    dst_data = data_blob.getPtr();
    std::cout << "start to forward" << std::endl;
    std::chrono::high_resolution_clock::time_point forwardstart = std::chrono::high_resolution_clock::now();
    net.forward();
    std::chrono::high_resolution_clock::time_point forwardend = std::chrono::high_resolution_clock::now();
    double forward_time = std::chrono::duration_cast<std::chrono::microseconds>(forwardend - forwardstart).count();
    printf("forward time=%.8lfms\n", forward_time / 1000);
    std::cout << "run finished" << std::endl;
    esnn::Blob result_blob = net.getBlob("prob");
    int result_wine = argmax(result_blob);
    std::cout << "wine classification result is " << result_wine << std::endl;
    net.clear();
    return result_wine;
  }
}
