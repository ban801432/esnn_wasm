## 在Web上对流酒视频进行酒花分类，程序中使用wasm格式的推理引擎ESNN推理酒花分类神经网络

### 一、直接使用已构建好的程序
1. 开启Web服务
```
python -m http.server 8020
cd 根目录/assets
```
2. 在浏览器中打开页面 http://localhost:8020/ 
选择classfication.html，拉到最下方，点击视频控件即可开始酒花分类。在上方canvas画布上可看到当前截取位置及分类标签，打开命令台即可看到推理模型相关信息。对于该酒花分类模型来说，0~5标签分别代表头酒、二段酒、二段转三段酒、三段酒、三段转酒尾酒、酒尾。

若成功推理模型，会依次输出网络结构信息、前传时间、最大概率的分类类别。
在本人mac上chrome浏览器上前传时间大约1146.66000000ms，分类标签为3，分类结果为“三段酒”。

### 二、手动构建
1. 编译工程

```
mkdir build
cd build
emcmake cmake ..
emmake make
```

2. 拷贝流酒视频等资源到指定文件夹下

```
cp 根目录/assets/favicon.ico 根目录/assets/out.mp4 根目录/build

```

3. 开启一个web服务测试

```
cd 根目录/build/
emrun --no_browser --port 8080 . 
```
也可以
> python -m http.server 8080

4. 在浏览器中打开页面 http://localhost:8080/ ，选择classfication.html，同上。

## LICENSE
Licensed under the MIT license
