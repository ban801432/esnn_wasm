// Author:Tun Ban
// Date:2022.6
#ifndef __INPUT_H
#define __INPUT_H

#include <fstream>
#include <sstream>

#include "layer.h"

namespace esnn
{

  class Input : public Layer
  {

  public:
    // functions
    Input(std::string name = "");
    virtual ~Input();
    virtual int loadParam(std::vector<std::string> params, int offset = 0);
    virtual int loadModel(std::ifstream &fp);
    virtual std::vector<std::vector<int>> inferShape(std::vector<std::vector<int>> bottom_shapes);
    virtual int forward(std::vector<Blob *> &bottoms, std::vector<Blob *> &tops);
    // variables
    std::vector<int> input_shape;
  };

} // namespace esnn

#endif //__INPUT_H