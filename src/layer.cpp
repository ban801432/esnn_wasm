// Author:Tun Ban
// Date:2022.6
#include "layer.h"

namespace esnn
{

      Layer::Layer(std::string name_) : name(name_), type("Abs") {}

      Layer::~Layer() {}

      int Layer::loadParam(std::vector<std::string> params, int offset)
      {
            return 0;
      }

      int Layer::loadModel(std::ifstream &fp)
      {
            return 0;
      }

      std::vector<std::vector<int>> Layer::inferShape(std::vector<std::vector<int>> bottom_shapes)
      {
      }

      int Layer::forward(std::vector<Blob *> &bottoms, std::vector<Blob *> &tops)
      {
            return 0;
      }

} // namespace esnn