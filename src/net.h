// Author:Tun Ban
// Date:2022.6
#ifndef __NET_H
#define __NET_H

#include "layer.h"

namespace esnn
{

  class Net
  {

  public:
    Net();
    ~Net();
    int loadParam(std::string filename);
    int loadParam(std::ifstream &fp);
    int loadModel(std::string filename);
    int loadModel(std::ifstream &fp);
    int forward();
    void clear();
    Blob &getBlob(std::string name);
    Blob &getBlob(int index);
    int getBlobIndex(std::string name);
    void printNetInfo();

  private:
    std::vector<Layer *> layers;
    std::vector<Blob *> blobs;
  };

} // namespace esnn

#endif //__NET_H